<?php
spl_autoload_register(function ($className){
    $folders = array(
        'program-files',
        'program-files/mvc',
        'program-files/Validators'
    );
    foreach ($folders as $folder) {
        $path = __DIR__ . "/{$folder}/" . $className . '.class.php';
        if (file_exists($path)) {
            require_once($path);
            return;
        }
    }
});