<?php

class ValidatorRange extends Validator
{
    protected $min = null;
    protected $max = null;

    public function __construct(int $min = null, int $max = null)
    {
        $this->min = $min;
        $this->max = $max;
    }

    public function validate($value, string $caption)
    {
        $mainText = "Значение \"{$caption}\"={$value} %s %s";
        parent::validate($value, $caption);
        if (!is_null($this->min) && $value < $this->min) {
            throw new Exception(sprintf($mainText, "меньше", $this->min));
        }
        if (!is_null($this->max) && $value > $this->max) {
            throw new Exception(sprintf($mainText, "больше", $this->max));
        }
    }
}