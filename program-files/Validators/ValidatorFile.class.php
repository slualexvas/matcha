<?php

class ValidatorFile
{
    protected $maxFileSize;
    protected $allowedFileExtensions;
    protected $allowedMimeTypes;

    public function __construct(int $maxFileSize = 500000, array $allowedFileExtensions = [], array  $allowedMimeTypes = [])
    {
        $this->maxFileSize = $maxFileSize;
        $this->allowedMimeTypes = $allowedMimeTypes;
        $this->allowedFileExtensions = $allowedFileExtensions;
    }

    public function validate(array $value, string $caption)
    {
        $this->validateFileSize($value, $caption);
        $this->validateFileExtension($value, $caption);
        $this->validateFileType($value, $caption);
    }

    protected function validateFileSize(array $value, string $caption)
    {
        if (empty($value['size'])) {
            throw new ValidatorException("Файл {$caption}: размер файла не указан либо пуст");
        }

        $fileSize = $value['size'];
        (new ValidatorInt())->validate($fileSize, "Размер файла {$caption}");

        if ($fileSize >= $this->maxFileSize) {
            throw new ValidatorException("Размер файла {$caption} равняется {$fileSize} байт и превышает максимально допустимый размер {$this->maxFileSize} байт");
        }
    }
    protected function validateFileType(array $value, string $caption)
    {
        $mimeType = mime_content_type($value['tmp_name']);
        $validatorHelper = new ValidatorEnum($this->allowedMimeTypes);
        $validatorHelper->validate($mimeType, "MIME-тип файла {$caption}");
    }
    protected function validateFileExtension(array $value, string $caption)
    {
        if (empty($value['name'])) {
            throw new ValidatorException("Файл {$caption}: не указано имя файла");
        }

        $fileName = $value['name'];
        if (($dotIndex = mb_strrpos($fileName,'.')) === false) {
            throw new ValidatorException("В имени файла {$fileName} нету точки. Это ошибка!");
        }

        $extension = mb_substr($fileName,$dotIndex + 1);
        $validatorHelper = new ValidatorEnum($this->allowedFileExtensions);
        $validatorHelper->validate($extension, "Расширение файла {$fileName}");
    }
}