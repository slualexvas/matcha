<?php

class ValidatorEnum extends Validator
{
    private $allowedValues = [];
    public function __construct(array $allowedValues)
    {
        $this->allowedValues = $allowedValues;
    }

    public function validate($value, string $caption)
    {
        parent::validate($value, $caption);

        if (empty($this->allowedValues)) {
            return;
        }

        if (!in_array($value, $this->allowedValues))
        {
            throw new ValidatorException("Value '{$caption}'=".htmlspecialchars($value)." is not allowed. Allowed values: " . join(', ', $this->allowedValues));
        }
    }
}