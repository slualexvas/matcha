<?php

class ValidatorFloat extends Validator
{
    public function validate($value, string $caption)
    {
        parent::validate($value, $caption);
        if (!is_numeric($value)) {
            throw new ValidatorException("Value '$caption' = '".htmlspecialchars($value)."' is not float");
        }
    }
}