<?php
function myIsInt($value) {
    return is_numeric($value) && floatval($value) == intval($value);
}

function myCheckDatetimeByFormat($value, $format) {
    $date = DateTime::createFromFormat($format, $value);
    return !empty($date) && ($value == ($date->format($format)));
}

function myIsDate($value) {
    return myCheckDatetimeByFormat($value, DATE_FORMAT) || $value == '0000-00-00';
}

function myIsDatetime($value) {
    return myCheckDatetimeByFormat($value, DATETIME_FORMAT) || $value == '0000-00-00 00:00:00';
}

function myPasswordEncrypt($value) {
    return password_hash($value, PASSWORD_DEFAULT);
}

function myJavascriptBackLinkHref() {
    return 'javascript:history.go(-1);';
}

function myMail($to,$subject,$message,$from='') {
    $message="<html><body>".$message."</body></html>";
    if (empty($from)) {
        $from = ADMIN_EMAIL;
    }
    
    $headers="Content-Type: text/html; charset=utf-8";
    $headers.="\r\n"."From: $from
		\r\n"."Reply-To: $from";
    if (!mail($to, $subject, $message, $headers))
    {
        $params = array(
            'to' => $to,
            'subject' => $subject,
            'message' => $message,
            'headers' => $headers
        );
        echo "Функция php mail(to, subject, message, headers) вернула false! Параметры: <pre>".htmlspecialchars(var_export($params, true))."</pre>";
    }
}

function myRandomCharFromString($s) {
    return mb_substr($s, rand(0, mb_strlen($s) - 1), 1);
}

function myPhpRedirect($href) {
    header("Location: {$href}",TRUE);
}

function myFormInput($name, $placeholder, array $attributes=[])
{
    $attributes['name'] = $name;
//
//    if (isset($attributes['value']) && is_numeric($attributes['value']) && isset($attributes['step'])) {
//        $roundArg = (int)log10($attributes['step']);
//        $attributes['value'] = round($roundArg);
//    }

    $attrString = '';
    foreach ($attributes as $key => $value) {
        $attrString .= " {$key}=\"{$value}\"";
    }
    return "
        <div class='form-group row'>
            <label for='{$name}' class='col-form-label col-sm-2'>{$placeholder}</label>
            <div class='col-sm-10'>
                <input class='form-control' {$attrString} />
            </div>
        </div>
    ";
}

function myFormInputSelect($name, $label, array $options = [], $selectedValue = null)
{
    $optionsString = '';
    foreach ($options as $value => $optLabel) {
        $selected = ($selectedValue === $value) ? 'selected' : '';
        $optionsString .= "
                    <option value='{$value}' {$selected}>{$optLabel}</option>";
    }

    $result = "
        <div class='form-group row'>
            <label for='{$name}' class='col-form-label col-sm-2'>{$label}</label>
            <div class='col-sm-10'>
                <select name='{$name}'>
                    $optionsString
                </select>
            </div>
        </div>
          ";
    return $result;
}

function myFormInputSelectMultiple(string $name, string $label, array $options = [], array $selectedValues = [], array $attributes = [])
{
    $optionsString = '';
    foreach ($options as $value => $optLabel) {
        $selected = in_array($value, $selectedValues) ? 'selected' : '';
        if ($selected) {
            $optionsString .= "
                    <option value='{$value}' {$selected}>{$optLabel}</option>";
        }
    }
    foreach ($options as $value => $optLabel) {
        $selected = in_array($value, $selectedValues) ? 'selected' : '';
        if (!$selected) {
            $optionsString .= "
                    <option value='{$value}' {$selected}>{$optLabel}</option>";
        }
    }

    $attributes['multiple'] = 'multiple';
    $attributes['size'] = min(15, count($options));
    $attrString = '';
    foreach ($attributes as $key => $value) {
        $attrString .= " {$key}=\"{$value}\"";
    }
    $result = "
        <div class='form-group row'>
            <input type='hidden' name='{$name}'/> <!-- Сработает в том случае, если в списке не выбрать ни одного варианта -->
            <label for='{$name}' class='col-form-label col-sm-2'>{$label}</label>
            <div class='col-sm-10'>
                <select name='{$name}' {$attrString}>
                    $optionsString
                </select>
            </div>
        </div>
          ";
    return $result;
}

function myFormInputTextarea($name, $placeholder, array $attributes=[])
{
    $attributes['name'] = $name;

    $fieldValue = $attributes['value'] ?? '';
    unset($attributes['value']);

    $attrString = '';
    foreach ($attributes as $key => $value) {
        $attrString .= " {$key}=\"{$value}\"";
    }
    return "
        <div class='form-group row'>
            <label for='{$name}' class='col-form-label col-sm-2'>{$placeholder}</label>
            <div class='col-sm-10'>
                <textarea class='form-control' {$attrString}>{$fieldValue}</textarea>
            </div>
        </div>
    ";
}

function myFormInputPhoto($name, $placeholder, array $attributes=[])
{
    $attributes['name'] = $name;

    $attrString = '';
    foreach ($attributes as $key => $value) {
        $attrString .= " {$key}=\"{$value}\"";
    }
    return "
        <div class='form-group row'>
            <label for='{$name}' class='col-form-label col-sm-2'>{$placeholder}</label>
            <div class='col-sm-2'>
                Текущее фото: ".(!empty($attributes['value']) ? "<img src='{$attributes['value']}' style='width: 3em;'>" : "(отсутствует)")."
            </div>
            <div class='col-sm-8'>
                <input class='form-control' type='file' {$attrString} />
            </div>
        </div>
    ";
}

function myGetGpsByGeoIp(string $ip):array
{
    $gps_x = 0;
    $gps_y = 0;

    $csvFile = DIR_ROOT . "/config/geoip-database.csv";
    $f = fopen($csvFile, "rt");
    while (($arr = fgetcsv($f, 0, ';')) !== false) {
        if (preg_match('/'.$arr[0].'/', $ip)) {
            $gps_x = $arr[1];
            $gps_y = $arr[2];
            break;
        }
    }
    fclose($f);

    return ['gps_x' => $gps_x, 'gps_y' => $gps_y];
}