<?php

class NotificationModel extends BaseModel
{
    public function storeNotification(int $notificatorId, int $notifiedId, $text)
    {
        $isBlockExists = (new UsersModel())->checkBlocking($notifiedId, $notificatorId);

        if (!$isBlockExists) {
            $sql = "INSERT INTO notification (user_id, text) VALUES ({$notifiedId}, {$this->db->quote($text)})";
            return $this->db->exec($sql);
        }
    }

    public function getNotifications(int $userId)
    {
        $sql = "SELECT * FROM notification WHERE user_id = {$userId} AND is_read = false ORDER BY n_time DESC";
        $notifications = $this->db->fetchAll($sql);

        $this->updateReadNotifications($userId, $notifications);

        return $notifications;
    }

    public function updateReadNotifications(int $userId, array &$notifications)
    {
        $ids = [];
        foreach ($notifications as &$notification) {
            if ($notification['user_id'] == $userId && $notification['is_read'] == false) {
                $ids[] = $notification['id'];
            }
        }
        if (!empty($ids)) {
            $this->db->exec("UPDATE notification SET is_read = true WHERE id IN (" . join(', ', $ids).")");
        }
    }

    public function getUnreadNotifications(int $userId) {
        return $this->db->fetchOne("SELECT COUNT(*) FROM notification WHERE user_id = {$userId} AND is_read = false");
    }
}