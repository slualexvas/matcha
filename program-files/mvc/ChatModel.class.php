<?php

class ChatModel extends BaseModel
{
    public function getUsersInfoForChat(int $myId, int $hisId):array
    {
        $usersModel = new UsersModel();
        return [
            'my_data' => $usersModel->getUserData($myId),
            'his_data' => $usersModel->getUserData($hisId),
        ];
    }

    public function getMessages(int $myId, int $hisId):array
    {
        $sql = "SELECT * FROM chat
                WHERE (id_from = {$myId} AND id_to = {$hisId})
                      OR (id_from = {$hisId} AND id_to = {$myId})
                ORDER BY time_of_send ASC";
        $messages = $this->db->fetchAll($sql);

        $this->updateMessagesDateOfRead($myId, $messages);
        return $messages;
    }

    public function updateMessagesDateOfRead(int $myId, array &$messages)
    {
        $ids = [];
        $now = date('Y-m-d H:i:s');
        foreach ($messages as &$message) {
            if ($message['id_to'] == $myId && $message['time_of_receive'] === null) {
                $ids[] = $message['id'];
                $message['time_of_receive'] = $now;
            }
        }
        if (!empty($ids)) {
            $this->db->exec("UPDATE chat SET time_of_receive = '$now' WHERE id IN (" . join(', ', $ids).")");
        }
    }

    public function sendMessage(int $idFrom, int $idTo, string $text)
    {
        $isBlockExists = (new UsersModel())->checkBlocking($idTo, $idFrom);
        if ($isBlockExists) {
            throw new Exception("Даный пользователь заблокировал Вас, отправка сообщений невозможна!");
        }

        $sql = "SELECT 1 FROM likes 
                WHERE id_liker = {$idFrom} 
                  AND id_liked = {$idTo}";
        $iLikeHim = !empty($this->db->fetchOne($sql));

        $sql = "SELECT 1 FROM likes 
                WHERE id_liked = {$idFrom} 
                  AND id_liker = {$idTo}";
        $heLikedMe = !empty($this->db->fetchOne($sql));

        if (!$iLikeHim || !$heLikedMe) {
            throw new Exception("Отправка сообщений невозможна. Чтобы он стал возможен, нужно, чтобы Вы лайкнули пользователя, и он лайкнул Вас.");
        }

        (new ValidatorCharWhiteList(ValidatorCharWhiteList::TEMPLATE_TEXT_WITHOUT_TAGS))->validate($text, "Текст сообщения");
        $sql = "INSERT INTO chat(id_from, id_to, message)
                VALUES ({$idFrom}, {$idTo}, {$this->db->quote($text)})";
        return $this->db->exec($sql);
    }

    public function getMyChatsInfo(int $userId)
    {
        $sql = "SELECT c.id_from AS his_id,
                       CONCAT(u.first_name, ' ', u.last_name) AS his_name,
                       COUNT(*) AS messages_unread
                FROM chat AS c
                  JOIN users AS u ON c.id_from = u.id
                WHERE c.id_to = {$userId} AND c.time_of_receive IS NULL
                GROUP BY c.id_from;
        ";
        return $this->db->fetchAll($sql);
    }

    public function getUnreadsCount(int $userId) {
        return $this->db->fetchOne("SELECT COUNT(*) FROM chat WHERE id_to = {$userId} AND time_of_receive IS NULL");
    }
}