<?php

class NotificationController extends BaseController
{
    private $model;
    private $view;

    public function __construct()
    {
        $this->model = new NotificationModel();
        $this->view = new BaseView();
    }

    public function actionNotification()
    {
        return $this->view->renderTemplate('notification.php', $this->model->getNotifications($this->getUserIdOrThrowExceptionIfEmpty()));
    }

    public function actionUnreadNotifications()
    {
        try {
            $userId = $this->getUserIdOrThrowExceptionIfEmpty();
            $result = $this->model->getUnreadNotifications($userId);
        } catch(Exception $e) {
            $result = 0;
        }
        $this->sendAjaxResponse(['count' => $result]);
    }
}