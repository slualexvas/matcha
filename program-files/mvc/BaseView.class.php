<?php

class BaseView
{
    public function renderTemplate($templateName, $params = array()) {
        ob_start();
        $fileName = DIR_ROOT.'/templates/'.$templateName;
        require (DIR_ROOT.'/templates/'.$templateName);
        $result = ob_get_contents();
        ob_end_clean();

        return $result;
    }
}