<?php

class TagsModel extends BaseModel
{
    public function indexTagsById($rows)
    {
        $result = [];
        foreach ($rows as $row) {
            $result[$row['id']] = $row['tag'];
        }
        return $result;
    }

    public function getAllTags()
    {
        return $this->indexTagsById($this->db->fetchAll("SELECT id, tag FROM tags"));
    }

    public function getTagsByUserId(int $userId):array
    {
        return $this->indexTagsById($this->db->fetchAll("
            SELECT t.id, t.tag
            FROM tags AS t
              JOIN users_tags_rel AS utr ON utr.id_tag = t.id
            WHERE utr.id_user = {$userId}
            "));
    }

    public function updateTagsExist(int $userId, array $tagsIds)
    {
        $this->db->exec("DELETE FROM users_tags_rel WHERE id_user = {$userId}");

        $insertValues = [];
        foreach ($tagsIds as $tagId) {
            if (!empty($tagId)) {
                $insertValues[] = "({$userId}, {$tagId})";
            }
        }
        if (!empty($insertValues)) {
            $this->db->exec("INSERT INTO users_tags_rel(id_user, id_tag) VALUES ".join(', ', $insertValues));
        }
    }
    public function updateTagsNew(int $userId, string $tagsNew)
    {
        $tags = explode(', ', $tagsNew);
        $tags = array_map('trim', $tags);
        $tags = array_map('mb_strtolower', $tags);
        $tags = array_unique($tags);
        $tags = array_filter($tags, function ($item){
            return !empty($item);
        });
        $tags = array_map([$this->db, 'quote'], $tags);

        if (empty($tags)) {
            return;
        }

        $insertValues = [];
        foreach ($tags as $tag) {
            $insertValues[] = "($tag)";
        }
        $this->db->exec("INSERT IGNORE INTO tags(tag) VALUES ".join(', ', $insertValues));

        $ids = $this->db->fetchCol("SELECT id FROM tags WHERE tag IN (".join(', ', $tags).")");

        $insertValues = [];
        foreach ($ids as $tagId) {
            $insertValues[] = "({$userId}, {$tagId})";
        }
        $this->db->exec("INSERT IGNORE INTO users_tags_rel(id_user, id_tag) VALUES ".join(', ', $insertValues));
    }
}