<?php

class UsersModel extends BaseModel
{
    const USER_LOGIN_KEY_IN_SESSION = 'user_login';
    const USER_ID_KEY_IN_SESSION = 'user_id';
    const PASSWORD_STARS = '********';
    const PHOTO_MAX_FILE_SIZE = 500000;

    const MIN_AGE = 16;
    const MAX_AGE = 80;

    const SORT_CONDITION_AGE    = 'age';
    const SORT_CONDITION_GPS    = 'gps';
    const SORT_CONDITION_TAGS   = 'tags';
    const SORT_CONDITION_RATING = 'rating';

    const KILOMETERS_BY_ONE_GPS_UNIT = 111;

    const genders = array(
        'bi' => "Бисексуал (хочу и девушек, и парней)",
        'male' => "Парень (хочу девушек)",
        'female' => "Девушка (хочу парней)",
    );

    const defaultPhotos = array(
        'male' => "/templates/img/no-photo-boy.jpg",
        'female' => "/templates/img/no-photo-girl.jpg",
        'bi' => "/templates/img/no-photo-bi.jpg",
    );

    private $currentUser = null;
    public function getCurrentUser()
    {
        if (!is_null($this->currentUser)) {
            return $this->currentUser;
        } else {
            $id = $this->getUserIdFromSession();
            return empty($id) ? null : $this->currentUser = $this->getUserData($id);
        }
    }

    public function login($login, $password)
    {
        $this->validate(array('login' => $login, 'password' => $password), array('login', 'password'));

        $sql = "SELECT * FROM users WHERE login = ?";

        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($login));
        $userData = $stmt->fetchAll();

        if (empty($userData)) {
            throw new ValidatorException((new UsersView())->getLoginFailedMessage($login, $password));
        }

        if (!password_verify($password, $userData[0]['password'])) {
            throw new ValidatorException((new UsersView())->getLoginFailedMessage($login, $password));
        }

        if (!$userData[0]['is_confirmed']) {
            throw new Exception((new UsersView())->renderTemplate("registrationFinalizeForm.php", $userData[0]));
        }

        $this->setUserLoginInSession($login);
        $this->setUserIdInSession((int)$userData[0]['id']);
    }

    public function setUserLoginInSession($login)
    {
        $_SESSION[self::USER_LOGIN_KEY_IN_SESSION] = $login;
    }

    public function getUserLoginFromSession()
    {
        return $_SESSION[self::USER_LOGIN_KEY_IN_SESSION] ?? '';
    }

    public function setUserIdInSession($id)
    {
        $_SESSION[self::USER_ID_KEY_IN_SESSION] = $id;
    }

    public function getUserIdFromSession()
    {
        return $_SESSION[self::USER_ID_KEY_IN_SESSION] ?? '';
    }

    public function logout()
    {
        unset($_SESSION[self::USER_LOGIN_KEY_IN_SESSION]);
        unset($_SESSION[self::USER_ID_KEY_IN_SESSION]);
    }

    public function validate($values, $requiredKeys)
    {
        $validatorLogin = new ValidatorCharWhiteList(ValidatorCharWhiteList::TEMPLATE_PARAM_NAME);
        $validatorText = new ValidatorCharWhiteList(ValidatorCharWhiteList::TEMPLATE_TEXT_WITHOUT_TAGS);
        $validatorEmail = new ValidatorCharWhiteList(ValidatorCharWhiteList::TEMPLATE_URL);
        $validatorPhoto = new ValidatorFile(self::PHOTO_MAX_FILE_SIZE, ['jpg'], ['image/jpeg']);
        $validatorTags = new ValidatorCharWhiteList(ValidatorCharWhiteList::CYRILLIC_LOWER . ValidatorCharWhiteList::CYRILLIC_UPPER . ValidatorCharWhiteList::LATIN_LOWER . ValidatorCharWhiteList::LATIN_UPPER . ",.'\" -_");
        $validatorInt = new ValidatorInt();
        $validatorFloat = new ValidatorFloat();

        $keys = [
            'login' => [$validatorLogin, new ValidatorMaxLen(64)],
            'password' => [$validatorText, new ValidatorMaxLen(64)],
            'email' => [$validatorEmail, new ValidatorMaxLen(64)],
            'last_name' => [$validatorText, new ValidatorMaxLen(64)],
            'first_name' => [$validatorText, new ValidatorMaxLen(64)],
            'sexual_preferences' => [$validatorText],
            'biography' => [$validatorText],
            'profile_photo_index' => [new ValidatorEnum([1, 2, 3, 4, 5])],
            'gender' => [new ValidatorEnum(array_keys(self::genders))],
            'tags_new' => [$validatorTags, new ValidatorMaxLen(64)],
            'gps_x' => [$validatorFloat, new ValidatorRange(-180, 180)],
            'gps_y' => [$validatorFloat, new ValidatorRange(-90, 90)],
            'age' => [$validatorInt, new ValidatorRange(self::MIN_AGE, self::MAX_AGE)],
        ];

        foreach ([1, 2, 3, 4, 5] as $i) {
            if (isset($values["photo{$i}"]) && !empty($values["photo{$i}"]['name'])) {
                $keys["photo{$i}"] = [$validatorPhoto];
            }
        }

        foreach ($keys as $key => $validators) {
            if (isset($values[$key])) {
                $value = $values[$key];
            } elseif (in_array($key, $requiredKeys)) {
                throw new ValidatorException("Не заполнено обязательное поле '{$key}'");
            } else {
                // значит, в массиве какое-то постороннее значение. TODO:  подумать, что с ним делать. Не трогать? Или удалять? (чтобы в insert не вошло)? Или переделать валидацию так, чтобы она возвращала значения?
                continue;
            }

            /** @var Validator $validator */
            foreach ($validators as $validator) {
                $validator->validate($value, $key);
            }

            if ($key == 'password') {
                $this->checkPasswordIsEnoughComplex($values['password']);
            }
        }
    }

    public function checkPasswordIsEnoughComplex($password)
    {
        $passwordMinLen = 8;
        $msg = "Пароль должен содержать не меньше {$passwordMinLen} символов, включая хотя бы одну большую латинскую букву и хотя бы одну цифру. Ваш пароль '{$password}' ";
        if (($passwordLen = mb_strlen($password)) < $passwordMinLen) {
            throw new ValidatorException($msg."содержит {$passwordLen} символов");
        } elseif (!preg_match("/[A-Z]/u", $password)) {
            throw new ValidatorException($msg."не содержит больших латинских букв");
        } elseif (!preg_match("/\d/u", $password)) {
            throw new ValidatorException($msg."не содержит цифр");
        }
    }

    public function register()
    {
        $this->validate($_POST, array('login', 'password', 'email'));

        $encryptedPassword = myPasswordEncrypt($_POST['password']);
        $sql = "INSERT INTO users(login, password, email) VALUES(
          ".$this->db->quote($_POST['login']).",
          ".$this->db->quote($encryptedPassword).",
          ".$this->db->quote($_POST['email'])."
        )";
        try {
            $this->db->exec($sql);
        } catch (DbException $e) {
            if (mb_stripos($e->getMessage(), "duplicate")) {
                throw new ValidatorException("Пользователь с логином <b>{$_POST['login']}</b> уже есть в базе данных!");
            } else {
                throw $e;
            }
        }
        
        $mailMessage = "Код для окончания регистрации: \r\n".$encryptedPassword;
        myMail($_POST['email'], "Окончание регистрации ".SITE_NAME, $mailMessage);
    }

    public function inputToken()
    {
        $this->validate($_POST, array('login'));

        $sql = "UPDATE users SET is_confirmed = 1 
                WHERE
                  login = {$this->db->quote($_POST['login'])}
                  AND password = {$this->db->quote($_POST['token'])}";
        if (!$this->db->exec($sql)) {//Если кол-во строк, затронутых запросом, равно нулю...
            throw new ValidatorException("Вы ввели неверный токен");
        }
    }

    public function resetPassword()
    {
        $this->validate($_POST, array('login', 'email'));

        $newPassword = $this->generateRandomPassword();
        $sql = "UPDATE users SET password = {$this->db->quote(myPasswordEncrypt($newPassword))}
                WHERE login = {$this->db->quote($_POST['login'])}
                      AND email = {$this->db->quote($_POST['email'])}";
        if (!$this->db->exec($sql)) {//Если не нашлось ни одной записи с таким логином и имэйлом...
            throw new ValidatorException("В базе данных нету пользователя
с логином <span class='invisible-text'>{$_POST['login']}</span>
и email <span class='invisible-text'>{$_POST['email']}</span> <i>(выделите, чтобы прочесть логин и email)</i>");
        }

        myMail($_POST['email'], "Новый пароль на ".SITE_NAME, $newPassword);
    }

    public function generateRandomPassword()
    {
        $result = '';

        $result .= myRandomCharFromString(ValidatorCharWhiteList::LATIN_UPPER);
        $result .= myRandomCharFromString(ValidatorCharWhiteList::DIGITS);
        for ($i = 0; $i < 6; $i++) {
            $result .= myRandomCharFromString(ValidatorCharWhiteList::DIGITS.ValidatorCharWhiteList::LATIN_LOWER.ValidatorCharWhiteList::LATIN_UPPER);
        }

        return $result;
    }

    public function changeUserData(int $id, array $values)
    {
        if ($values['password'] === self::PASSWORD_STARS) {
            unset($values['password']);
        }

        if ($values['gps_x'] == 0) {
            unset($values['gps_x']);
        }

        if ($values['gps_y'] == 0) {
            unset($values['gps_y']);
        }

        $this->validate(array_merge($values, $_FILES), array('login', 'email'));
        $this->updateUserPhotos($_FILES);
        unset($values['MAX_FILE_SIZE']);

        if (isset($values['tags_exist'])) {
            (new TagsModel())->updateTagsExist($id, $values['tags_exist']);
            unset($values['tags_exist']);
        }
        if (isset($values['tags_new'])) {
            (new TagsModel())->updateTagsNew($id, $values['tags_new']);
            unset($values['tags_new']);
        }

        $setFields = array();
        foreach ($values as $key => $value) {
            if ($key == 'password') {
                $value = myPasswordEncrypt($value);
            }
            $setFields[] = "{$key} = {$this->db->quote($value)}";
        }
        try {
            $sql = "UPDATE users SET " . join(', ', $setFields) . " WHERE id = {$id}";
            $this->db->exec($sql);
        } catch (DbException $e) {
            if (mb_stripos($e->getMessage(), 'duplicate') !== false) {
                throw new ValidatorException("Пользователь с логином <b>{$values['login']}</b> уже есть в базе данных!");
            } else {
                throw $e;
            }
        }
        $this->recalcFameRating("id = {$id}");
    }

    public function updateUserPhotos(array $files)
    {
        $dirToUpload = $this->getUserPhotosDir($this->getUserIdFromSession());
        if (!file_exists($dirToUpload)) {
            mkdir($dirToUpload);
            echo `chmod 0777 {$dirToUpload}`;
        }

        foreach ([1, 2, 3, 4, 5] as $i) {
            if (empty($files['photo' . $i]['tmp_name'])) {
                continue;
            }

            if (!move_uploaded_file($files['photo' . $i]['tmp_name'], $dirToUpload . "{$i}.jpg")) {
                throw new Exception("Не сработало move_uploaded_file(" .$files['photo' . $i]['tmp_name'] . ", " .$dirToUpload . "{$i}.jpg)");
            }
//            else {
//                echo "move_uploaded_file(" .$files['photo' . $i]['tmp_name'] . ", " .$dirToUpload . "{$i}.jpg)<br>";
//            }
        }
    }

    public function getUserData($id)
    {
        $result = $this->db->fetchRow("SELECT * FROM users WHERE id = {$id}");
        if (empty($result)) {
            throw new Exception("В базе нет пользователя с id = {$id}");
        }
        $this->postProcessUserData($result);
        return $result;
    }

    private function getFullName($userData)
    {
        $firstName = $userData['first_name'] ?? '';
        $lastName = $userData['last_name'] ?? '';

        if (empty($firstName) && empty($lastName)) {
            $result = $userData['login'];
        } else {
            $result = "{$firstName} {$lastName}";
        }

        $result .= " ({$userData['age']} лет)";

        if (!empty($userData['is_this_i'])) {
            $result .= "<b>(Это я!)</b>";
        }

        return $result;
    }

    private function postProcessUserData(&$userData)
    {
        $userData = array_merge($userData, $this->getUserPhotos($userData));
        $userData['tags'] = (new TagsModel())->getTagsByUserId($userData['id']);

        $sql = "SELECT 
                  vh.id_viewer, 
                  vh.time_of_viewing,
                  CONCAT(CASE WHEN u.first_name = '' THEN '(Имя не указано)' ELSE u.first_name END, ' ', u.last_name) AS 'name'
                FROM view_history AS vh
                  JOIN users AS u ON vh.id_viewer = u.id
                WHERE vh.id_viewed = {$userData['id']} AND vh.time_of_viewing > NOW() - INTERVAL 1 DAY
                ORDER BY vh.time_of_viewing DESC";
        $userData['view_history'] = $this->db->fetchAll($sql);

        $userIdFromSession = $this->getUserIdFromSession();

        $userData['is_this_i'] = $userIdFromSession == $userData['id'];

        if (empty($userIdFromSession)) {
            $userData['i_liked_him'] = false;
            $userData['he_liked_me'] = false;

            $userData['common_tags'] = [];
        } elseif ($userData['is_this_i']) {
            $userData['i_liked_him'] = false;
            $userData['he_liked_me'] = false;

            $userData['common_tags'] = $userData['tags'];
            $userData['distance_from_me'] = 0;
        } else {
            $sql = "SELECT 1 FROM likes 
                WHERE id_liker = {$userIdFromSession} 
                  AND id_liked = {$userData['id']}";
            $userData['i_liked_him'] = !empty($this->db->fetchOne($sql));

            $sql = "SELECT 1 FROM likes 
                WHERE id_liked = {$userIdFromSession} 
                  AND id_liker = {$userData['id']}";
            $userData['he_liked_me'] = !empty($this->db->fetchOne($sql));

            $userData['i_block_him'] = $this->checkBlocking($userIdFromSession, $userData['id']);

            $currentUser = $this->getCurrentUser();
            $userData['common_tags'] = array_intersect($userData['tags'], $currentUser['tags']);

            $userData['distance_from_me'] = UsersModel::KILOMETERS_BY_ONE_GPS_UNIT * sqrt(pow($userData['gps_x'] - $currentUser['gps_x'], 2) + pow($userData['gps_y'] - $currentUser['gps_y'], 2));
            $userData['distance_from_me'] = round($userData['distance_from_me'], 3);

            $userData['can_i_like'] = ($currentUser['count_photos'] > 0);
        }

        $userData['chat_is_possible'] = $userData['i_liked_him'] && $userData['he_liked_me'];

        $userData['count_likes'] = $this->db->fetchOne("SELECT COUNT(*) FROM likes WHERE id_liked = {$userData['id']}");
        $userData['full_name'] = $this->getFullName($userData);
    }

    public function recalcFameRating(string $whereCondition = 'TRUE')
    {
        $userList = $this->getUserList($whereCondition);
        foreach ($userList as $user) {
            $fameRating = $user['count_likes'] + 0.5 * $user['count_photos'];
            $this->db->exec("UPDATE users SET fame_rating = {$fameRating} WHERE id = {$user['id']}");
        }
    }

    private function getUserPhotosDir($id)
    {
        return DIR_ROOT . "/files/{$id}/";
    }

    private function getUserPhotos(array $userData)
    {
        $result = [
            'photo1' => null,
            'photo2' => null,
            'photo3' => null,
            'photo4' => null,
            'photo5' => null,
            'photo_main' => null,
        ];

        $result['count_photos'] = 0;
        $dir = $this->getUserPhotosDir($userData['id']);
        if (file_exists($dir)) {
            foreach ([1, 2, 3, 4, 5] as $i) {
                $file = $dir . "{$i}.jpg";
                if (file_exists($file)) {
                    $result["photo{$i}"] = $file;
                    $result['count_photos']++;
                }
            }
        } else {
            $result['photo_main'] = $result['photo1'] = self::defaultPhotos[$userData['gender']];
        }

        $result['photo_main'] = $result["photo{$userData['profile_photo_index']}"] ?? self::defaultPhotos[$userData['gender']];

        if (is_null($result["photo{$userData['profile_photo_index']}"])) {
            $result["photo{$userData['profile_photo_index']}"] = $result['photo_main'];
        }

        foreach ($result as $key => $value) {
            if (!empty($value)) {
                $result[$key] = str_replace(DIR_ROOT, '', $value);
            }
        }

        return $result;
    }

    public function getUserList(string $whereCondition = 'TRUE'):array
    {
        $rows = $this->db->fetchAll("SELECT * FROM users WHERE {$whereCondition}");
        foreach ($rows as &$row) {
            $this->postProcessUserData($row);
        }
        return $rows;
    }

    public function addViewHistoryItem(int $viewerId, int $viewedId)
    {
        if ($viewerId != $viewedId) {//Чтобы не засорять историю просмотров
            $store = $this->db->exec("INSERT IGNORE INTO view_history(id_viewer, id_viewed, time_of_viewing) VALUES ({$viewerId}, {$viewedId}, FROM_UNIXTIME(UNIX_TIMESTAMP() div 300 * 300))");// Вставляем время просмотра, округлённое до 5 минут, чтобы не засорять историю частыми просмотрами каждую секунду

            if ($store) {
                $viewer = $this->getUserData($viewerId);
                (new NotificationModel())->storeNotification($viewerId, $viewedId, "Пользователь {$viewer['first_name']} {$viewer['last_name']} просмотрел ваш профиль");
            }
        }
    }

    public function like(int $likerId, int $likedId)
    {
        $isLikeExists = !empty($this->db->exec("DELETE FROM likes WHERE id_liker = {$likerId} AND id_liked = {$likedId}"));
        $act = "дизлайк";

        if (!$isLikeExists) {
            $act = "лайк";
            $this->db->exec("INSERT INTO likes(id_liker, id_liked) VALUES({$likerId}, {$likedId})");
        }

        $liker = $this->getUserData($likerId);
        (new NotificationModel())->storeNotification($likerId, $likedId, "Пользователь {$liker['first_name']} {$liker['last_name']} поставил вам {$act}");

        $this->recalcFameRating("id = {$likedId}");
    }

    public function block(int $idBlocker, int $idBlocked)
    {
        $isBlockExists = !empty($this->db->exec("DELETE FROM users_block WHERE id_blocker = {$idBlocker} AND id_blocked = {$idBlocked}"));
        $act = "разблокировал";

        if (!$isBlockExists) {
            $act = "заблокиировал";
            $this->db->exec("INSERT INTO users_block(id_blocker, id_blocked) VALUES({$idBlocker}, {$idBlocked})");
        }

        $user = $this->getUserData($idBlocker);
        (new NotificationModel())->storeNotification($idBlocker, $idBlocked, "Пользователь {$user['first_name']} {$user['last_name']} {$act} вас");

        $this->recalcFameRating("id = {$idBlocked}");
    }

    public function checkBlocking($idBlocker, $idBlocked){
        $sql = "SELECT 1 FROM users_block 
                WHERE id_blocker = {$idBlocker} 
                  AND id_blocked = {$idBlocked}";
        $result = !empty($this->db->fetchOne($sql));

        return $result;
    }

    public function getProposedUsers(array $params):array
    {
        $sqlBuilder = new ProposedUsersSQLBuilder($params);
        $sql = $sqlBuilder->getProposedUsersSQL();

        $ids = $this->db->fetchCol($sql);
        $users = empty($ids) ? [] : $this->getUserList("id IN (" . join(', ', $ids) . ")");

        $sortedUsers = [];
        foreach ($ids AS $id) {
            foreach ($users as $user) {
                if ($user['id'] == $id && $user['i_block_him'] == false) {
                    $sortedUsers[] = $user;
                    continue;
                }
            }
        }
        return $sortedUsers;
    }

    public function updateLastActive($id)
    {
        $this->db->exec("UPDATE users SET last_activity = CURRENT_TIMESTAMP WHERE id = {$id}");
    }
}