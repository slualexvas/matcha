<?php
class UsersController extends BaseController
{
    /** @var UsersModel */
    private $model = null;

    /** @var UsersView */
    private $view = null;
    public function __construct()
    {
        $this->model = new UsersModel();
        $this->view = new UsersView();
    }

    public function strLoginForm()
    {
        $authorizedUserLogin = $this->model->getUserLoginFromSession();
        return empty($authorizedUserLogin) ? $this->view->strLoginFormIfNotAuthorized() : $this->view->strLoginFormIfAuthorized($authorizedUserLogin);
    }

    public function actionLogin()
    {
        $login = $_POST['login'] ?? '';
        $password = $_POST['password'] ?? '';

        $this->model->login($login, $password);
        return "Вы успешно залогинились! {$this->linkToMyProfile()}";
    }

    public function actionLogout()
    {
        $this->model->logout();
        return "Вы успешно разлогинились!";
    }

    public function actionRegisterForm()
    {
        return $this->view->renderTemplate("registerForm.php");
    }

    public function actionRegister()
    {
        $this->model->register();
        return $this->view->renderTemplate("registrationFinalizeForm.php", $_POST);
    }

    public function actionInputToken()
    {
        $this->model->inputToken();
        return "Ваш аккаунт подтверждён. Можете логиниться и работать.";
    }

    public function actionResetPasswordForm()
    {
        return $this->view->renderTemplate("resetPasswordForm.php");
    }

    public function actionResetPassword()
    {
        $this->model->resetPassword();
        return "Пароль успешно изменён! Вам на почту отправлено письмо с новым паролем.";
    }

    public function actionChangeUserDataForm()
    {
        $userId = $this->getUserIdOrThrowExceptionIfEmpty();
        $params = $this->model->getUserData($userId);
        $params['password'] = UsersModel::PASSWORD_STARS;
        return ($this->view->renderTemplate('changeUserDataForm.php', $params));
    }

    public function actionChangeUserData()
    {
        $id = $this->getUserIdOrThrowExceptionIfEmpty();
        $this->model->changeUserData($id, $_POST);
        return "Ваши данные успешно изменены. {$this->linkToMyProfile()}";
    }
    
    public function actionProfile()
    {
        $id = (int)($_GET['id'] ?? 0);
        $myId = $this->getUserIdOrThrowExceptionIfEmpty();
        if (empty($id)) {
            $id = $myId;
        }
        $this->model->addViewHistoryItem($myId, $id);
        $params = $this->model->getUserData($id);
        $params['is_my_profile'] = ($id == $myId);
        return $this->view->renderTemplate('profile.php', $params);
    }

    public function actionList()
    {
        return $this->view->renderTemplate('userList.php', ['user_list' => $this->model->getUserList()]);
    }

    public function actionLike()
    {
        $likedId = (int)$_GET['id'];
        $likerId = $this->getUserIdOrThrowExceptionIfEmpty();

        $this->model->like($likerId, $likedId);
        myPhpRedirect(
            "/index.php?controller=users&action=profile&id={$likedId}#like_form"
        );
    }

    public function actionBlock()
    {
        $idBlocker = $this->getUserIdOrThrowExceptionIfEmpty();
        $idBlocked = (int)$_GET['id'];

        $this->model->block($idBlocker, $idBlocked);
        myPhpRedirect(
            "/index.php?controller=users&action=profile&id={$idBlocked}"
        );
    }

    public function actionComplain()
    {
        $id = $this->getUserIdOrThrowExceptionIfEmpty();
        $from = $this->model->getUserData($id);
        $id = $_GET['id'];
        $to = $this->model->getUserData($id);

        $message = "В проекте Matcha пользователь {$from['first_name']} {$from['last_name']} пожаловался на {$to['first_name']} {$to['last_name']}.";
        $to  = "<angell2006@ukr.net>" ;

        $subject = "Жалоба";

        myMail($to, $subject, $message);
        return "Ваша жалоба отправлена.";
    }

    public function actionPropose()
    {
        $this->getUserIdOrThrowExceptionIfEmpty();

        $params = [
            'sort_condition' => UsersModel::SORT_CONDITION_RATING,
            'tags_search' => null,
            'min_rating' => 0,
            'max_distance_in_km' => 40000,
            'min_age' => 16,
            'max_age' => 80,
            'min_count_common_tags' => 0,
        ];

        foreach ($params as $key => $value) {
            if (isset($_GET[$key])) {
                $params[$key] = $_GET[$key];
            }
        }

        return $this->view->renderTemplate('userPropose.php', ['user_list' => $this->model->getProposedUsers($params)]);
    }

    public function linkToMyProfile()
    {
        $userId = $this->getUserIdOrThrowExceptionIfEmpty();
        return "<a href='/index.php?controller=users&action=profile&id={$userId}'>Просмотреть ваш профиль</a>";
    }

    public function actionLastActive()
    {
        $id = (int)$_GET['id'];
        $this->model->updateLastActive($id);
    }
}