<?php
class UsersView extends BaseView
{
    public function strLoginFormIfNotAuthorized()
    {
        return $this->renderTemplate('loginFormIfNotAuthorized.php');
    }

    public function strLoginFormIfAuthorized($login)
    {
        $params = array('login' => $login);
        return $this->renderTemplate('loginFormIfAuthorized.php', $params);
    }
    
    public function getLoginFailedMessage($login, $password)
    {
        return "В базе данных нету пользователя
с логином <span class='invisible-text'>{$login}</span>
и паролем <span class='invisible-text'>{$password}</span> <i>(выделите, чтобы прочесть логин и пароль)</i>";
    }

    public function getOnlineStatus($last_active)
    {
        return strtotime('now') - strtotime($last_active) < 60 ? "Онлайн" : "оффлайн с {$last_active}";
    }

    public function getLikeButton($userData)
    {
        $buttonCaption = $userData['i_liked_him'] ? 'Дизлайк' : 'Лайк';
        return "
            <form style='display: inline-block' method='post' id='like_form'
                action='/index.php?controller=users&action=like&id={$userData['id']}'>
                <input type='submit' value='{$buttonCaption}'/>
            </form>
        ";
    }

    public function getChatButton($userData)
    {
        if (!$userData['chat_is_possible']) {
            return "Чат невозможен. Чтобы он стал возможен, нужно, чтобы Вы лайкнули пользователя, и он лайкнул Вас.";
        }

        return "
            <a href='/index.php?controller=chat&action=view&user_id={$userData['id']}'
                class='btn btn-success'>
            Перейти к чату</a>
        ";
    }

    public function getComplainButton($userData)
    {
        return "
            <a href='/index.php?controller=users&action=complain&id={$userData['id']}'
                class='btn btn-secondary'>
            Пожаловатся</a>
        ";
    }

    public function getBlockButton($userData)
    {
        $buttonCaption = $userData['i_block_him'] ? 'Разблокировать' : 'Заблокировать';
        return "
            <a href='/index.php?controller=users&action=block&id={$userData['id']}'
                class='btn btn-secondary'>
            {$buttonCaption}</a>
        ";
    }

    public function getViewHistory($userData)
    {
        $views = [];
        foreach ($userData['view_history'] as $row) {
            $views[] = "<a href='/index.php?controller=users&action=profile&id={$row['id_viewer']}'>{$row['name']}</a> (в {$row['time_of_viewing']})";
        }
        return join(', ', $views);
    }
}