<?php

class ChatController extends BaseController
{
    private $model;
    private $view;

    public function __construct()
    {
        $this->model = new ChatModel();
        $this->view = new ChatView();
    }

    public function actionView()
    {
        $hisId = (int)$_GET['user_id'];
        $myId = $this->getUserIdOrThrowExceptionIfEmpty();

        $params = $this->model->getUsersInfoForChat($myId, $hisId);
        return $this->view->renderTemplate('chat.php', $params);
    }

    public function actionGetMessages()
    {
        $hisId = (int)$_GET['user_id'];
        $myId = $this->getUserIdOrThrowExceptionIfEmpty();
        $this->sendAjaxResponse($this->model->getMessages($myId, $hisId));
    }

    public function actionSendMessage()
    {
        try {
            $hisId = (int)$_GET['user_id'];
            $myId = $this->getUserIdOrThrowExceptionIfEmpty();
            $text = $_POST['text'];

            $this->model->sendMessage($myId, $hisId, $text);
            $result = ['status' => true];
        } catch (Exception $e) {
            $result = ['status' => false, 'message' => $e->getMessage()];
        }
        $this->sendAjaxResponse($result);
    }

    public function actionMyChats()
    {
        return $this->view->renderTemplate('myChats.php', $this->model->getMyChatsInfo($this->getUserIdOrThrowExceptionIfEmpty()));
    }

    public function actionUnreadsCount()
    {
        try {
            $userId = $this->getUserIdOrThrowExceptionIfEmpty();
            $result = $this->model->getUnreadsCount($userId);
        } catch(Exception $e) {
            $result = 0;
        }
        $this->sendAjaxResponse(['count' => $result]);
    }
}