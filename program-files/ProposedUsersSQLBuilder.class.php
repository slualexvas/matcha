<?php

class ProposedUsersSQLBuilder
{
    /* @var $usersModel UsersModel */
    private $usersModel;
    private $currentUser;

    private $params;
    const PARAMS_KEYS = [
        'sort_condition',
        'min_rating',
        'max_distance_in_km',
        'min_age',
        'max_age',
        'min_count_common_tags'
    ];

    private $sqlFields = [];
    private $sqlWhere = [];
    private $sqlHaving = [];
    private $sqlOrderBy = [];
    private $sqlJoins = [];

    function __construct(array $params)
    {
        $this->usersModel = new UsersModel();
        $this->currentUser = $this->usersModel->getCurrentUser();

        $this->params = $params;
        $this->validateParams();
    }

    function validateParams()
    {
        $validatorSortCondition = new ValidatorEnum([
                UsersModel::SORT_CONDITION_GPS,
                UsersModel::SORT_CONDITION_TAGS,
                UsersModel::SORT_CONDITION_AGE,
                UsersModel::SORT_CONDITION_RATING,
        ]);

        $keysToValidators = [
            'sort_condition' => [$validatorSortCondition],
            'min_rating' => [new ValidatorInt(), new ValidatorRange(0, null)],
            'max_distance_in_km' => [new ValidatorInt(), new ValidatorRange(0, null)],
            'min_age' => [new ValidatorInt(), new ValidatorRange(UsersModel::MIN_AGE, UsersModel::MAX_AGE)],
            'max_age' => [new ValidatorInt(), new ValidatorRange(UsersModel::MIN_AGE, UsersModel::MAX_AGE)],
            'min_count_common_tags' => [new ValidatorInt(), new ValidatorRange(0, null)],
        ];

        foreach ($keysToValidators as $key => $validators) {
            /** @var Validator $validator */
            foreach ($validators as $validator) {
                $validator->validate($this->params[$key] ?? null, $key);
            }
        }

        $this->validateTagsSearch();
    }

    function validateTagsSearch()
    {
        $tagsIds = array_keys((new TagsModel())->getAllTags());
        $validator = new ValidatorEnum($tagsIds);

        if (empty($this->params['tags_search'])) {
            $this->params['tags_search'] = [];
        }

        foreach ($this->params['tags_search'] as $key => $value) {
            if (empty($value)) {
                unset($this->params['tags_search'][$key]);
            } else {
                $validator->validate($value, 'id тэга');
            }
        }
    }

    public function getProposedUsersSQL()
    {
        $this->buildSQLParams();
        $this->currentUser['tags'][] = [0 => '(Пустой тег)'];

        return "
            SELECT 
                  " . join(",\n\t", $this->sqlFields) . "
            FROM users AS u
                  " . join("\n\t ", $this->sqlJoins) . "
            WHERE " . join("\n\t AND ", $this->sqlWhere) . "
            GROUP BY u.id
            HAVING " . join("\n\t AND ", $this->sqlHaving) . "
            ORDER BY " . join(', ', $this->sqlOrderBy) . "
            LIMIT 3
        ";
    }

    private function buildSQLParams()
    {
        $this->buildSQLOrderBy();

        $this->applyGender();

        $this->sqlFields = [
            "u.id",
            "{$this->getDistanceSQL()} AS distance",
        ];

        $this->sqlWhere[] = "u.fame_rating >= {$this->params['min_rating']}";
        $this->applyMaxDistanceInKm();
        $this->sqlWhere[] = "u.age BETWEEN {$this->params['min_age']} AND {$this->params['max_age']}";

        if (!empty($this->currentUser['tags'])) {
            $this->sqlFields[] = "COUNT(utr.id_tag) AS count_common_tags";
            $this->sqlJoins[] = "LEFT JOIN users_tags_rel AS utr ON utr.id_user = u.id AND utr.id_tag IN (".join(', ', array_keys($this->currentUser['tags'])).")";
        } else {
            $this->sqlFields[] = "0 AS count_common_tags";
        }
        $this->sqlHaving[] = "count_common_tags >= {$this->params['min_count_common_tags']}";

        if (!empty($this->params['tags_search'])) {
            $this->sqlFields[] = "COUNT(utr2.id_tag) AS count_allowed_tags";
            $this->sqlHaving[] = "count_allowed_tags > 0";
            $this->sqlJoins[] = "LEFT JOIN users_tags_rel AS utr2 ON utr2.id_user = u.id AND utr2.id_tag IN (" . join(', ', $this->params['tags_search']) . ")";
        }
    }

    private function buildSQLOrderBy()
    {
        $sortConditionToOrderBy = [
            UsersModel::SORT_CONDITION_AGE => 'u.age',
            UsersModel::SORT_CONDITION_GPS => 'distance',
            UsersModel::SORT_CONDITION_TAGS => 'count_common_tags DESC',
            UsersModel::SORT_CONDITION_RATING => 'u.fame_rating DESC',
        ];

        $this->sqlOrderBy[] = $sortConditionToOrderBy[$this->params['sort_condition']];
    }

    private function applyGender()
    {
        switch ($this->currentUser['gender']) {
            case 'male': $this->sqlWhere[] = "u.gender = 'female'";break;
            case 'female': $this->sqlWhere[] = "u.gender = 'male'";break;
        }
    }

    private function applyMaxDistanceInKm()
    {
        $maxDistanceInKm = $this->params['max_distance_in_km'];
        $maxDistanceInGPS = $maxDistanceInKm / UsersModel::KILOMETERS_BY_ONE_GPS_UNIT;
        $maxDistanceInGPSSquare = pow($maxDistanceInGPS, 2); //чтобы внутри sql не брать квадратный корень

        $this->sqlHaving[] = "distance <= {$maxDistanceInGPSSquare}";
    }

    private function getDistanceSQL()
    {
        $gpsDiffX = "u.gps_x - {$this->currentUser['gps_x']}";
        $gpsDiffY = "u.gps_y - {$this->currentUser['gps_y']}";
        return "POW({$gpsDiffX}, 2) + POW({$gpsDiffY}, 2)";
    }
}