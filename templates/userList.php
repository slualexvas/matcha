<h1>Список пользователей</h1>
<?php $isAuthorized = !empty((new UsersModel())->getUserIdFromSession()); ?>
<table class="table">
    <thead>
        <th>Фото</th>
        <th>Имя</th>
        <th>Гендер</th>
        <th>Рейтинг</th>
        <th>Интересы</th>
        <?php if ($isAuthorized) {
            echo "
        <th>Общие (с моими) интересы</th>
        <th>Расстояние от меня</th>";
        } ?>
        <th>Действия</th>
    </thead>
<?php

foreach ($params['user_list'] as $user) {
    $photo = $user['photo_main'];
    $gender = UsersModel::genders[$user['gender']];
    $actions = [];

    if ($isAuthorized) {
        $actions[] = "<a href='/index.php?controller=users&action=profile&id={$user['id']}'>Просмотреть профиль</a>";
    }
    echo "
    <tr>
        <td style='text-align: right'><img src='{$photo}' style='height: 6em;'></td> <!-- Фото -->
        <td>{$user['full_name']}</td> <!-- Имя -->
        <td>{$gender}</td> <!-- Гендер -->
        <td>{$user['fame_rating']}</td> <!-- Рейтинг -->
        <td>".join(', ', $user['tags'])."</td> <!-- Интересы -->";
    if ($isAuthorized) {
        echo "
        <td>".join(', ', $user['common_tags'])."</td> <!-- Общие (с моими) интересы -->
        <td>{$user['distance_from_me']} км</td> <!-- Расстояние от меня -->";
    }
    echo "
        <td>
            ". join(",\n", $actions) ."
         </td> <!-- Действия -->
    </tr>
    ";
}

?>
</table>
