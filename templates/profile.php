<?php
$user = $params;
$view = new UsersView();

echo "<h1>Профиль пользователя \"{$user['full_name']}\"</h1>";
echo "<h5>{$view->getOnlineStatus($user['last_activity'])}</h5>";

$genderKey = $user['gender'] ?? 'bi';

$gender = UsersModel::genders[$genderKey];
?>

<div class="photos">
    <?php
    foreach ([1, 2, 3, 4, 5] as $i) {
        $src = $user["photo{$i}"];
        if (!empty($src)) {
            echo "<img src='{$src}' class='col-sm-2'>";
        }
    }
    ?>
</div>

<table class="table">
    <tr>
        <th style="width: 30%">Рейтинг</th>
        <td style="width: 70%"><?php echo $user['fame_rating']; ?></td>
    </tr>
    <tr>
        <th style="width: 30%">Гендер</th>
        <td style="width: 70%"><?php echo $gender; ?></td>
    </tr>
    <tr>
        <th>Сексуальные предпочтения</th>
        <td><?php echo $user['sexual_preferences']; ?></td>
    </tr>
    <tr>
        <th>Биография</th>
        <td><?php echo $user['biography']; ?></td>
    </tr>
    <tr>
        <th>Интересы (tags):</th>
        <td><?php
                echo join(', ', $user['tags']);
            ?></td>
    </tr>

    <?php
        if ($user['is_this_i']) {
            echo "
                <tr>
                    <th>Кто смотрел мою анкету (за последние сутки):</th>
                    <td>{$view->getViewHistory($user)}</td>
                </tr>
            ";
        }
    ?>

    <?php
    if (!$user['is_this_i']) {
        echo "
                <tr>
                    <th>Общие интересы с моими</th>
                    <td>".join(', ', $user['common_tags'])."</td>
                </tr>
                <tr>
                    <th>Расстояние от меня</th>
                    <td>{$user['distance_from_me']} км</td>
                </tr>
                <tr>
                    <th>Лайкнул ли этот пользователь меня?</th>
                    <td>".($user['he_liked_me'] ? 'Да' : 'Нет')."</td>
                </tr>
                <tr>
                    <th>Лайкнули ли я этого пользователя?</th>
                    <td>".($user['i_liked_him'] ? 'Да' : 'Нет').($user['can_i_like'] ? $view->getLikeButton($user) : ' (для того чтобы поставить лайк Вам необходимо загрузить хотя-бы одну фотографию!)')."</td>
                </tr>
                <tr>
                    <th>Чат</th>
                    <td>{$view->getChatButton($user)}</td>
                </tr>
                <tr>
                    <th>Пожаловаться на пользователя</th>
                    <td>{$view->getComplainButton($user)}</td>
                </tr>
                <tr>
                    <th>Заблокировать пользователя</th>
                    <td>{$view->getBlockButton($user)}</td>
                </tr>
            ";
    }
    ?>
</table>
