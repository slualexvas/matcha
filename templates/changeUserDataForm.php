<form method="post" action="/index.php?controller=users&action=changeUserData" enctype="multipart/form-data">
    <h1>Форма смены данных (<?php echo $params['login']; ?>)</h1>

    <?php
        $formInputs = [
            myFormInput('login', 'Ваш логин', ['type' => 'text', 'value' => $params['login']]),
            myFormInput('password', 'Ваш пароль', ['type' => 'password', 'value' => $params['password']]),
            myFormInput('email', 'Ваш email', ['type' => 'email', 'value' => $params['email']]),
            myFormInput('first_name', 'Ваше имя', ['value' => $params['first_name']]),
            myFormInput('last_name', 'Ваша фамилия', ['value' => $params['last_name']]),
            myFormInputSelect('gender', 'Ваш гендер', UsersModel::genders, $params['gender']),
            myFormInputTextarea('sexual_preferences', 'Ваши сексуальные предпочтения', ['value' => $params['sexual_preferences']]),
            myFormInputTextarea('biography', 'Ваша биография', ['value' => $params['biography']]),
            myFormInputSelect('profile_photo_index', 'Номер основной фотографии', [1=>1, 2=>2, 3=>3, 4=>4, 5=>5], (int)$params['profile_photo_index']),
            "<input type='hidden' name='MAX_FILE_SIZE' value='".UsersModel::PHOTO_MAX_FILE_SIZE."' />",
            myFormInputSelect('i_agree_to_read_my_gps', 'Я согласен, чтобы читали мои GPS-координаты', [1=>'Да', 0=>'Нет'], (int)$params['i_agree_to_read_my_gps']),
            myFormInputSelectMultiple('tags_exist[]', 'Интересы (tags) -- выберите уже существующий', (new TagsModel())->getAllTags(), array_keys($params['tags'])),
            myFormInputTextarea('tags_new', 'Интересы (tags) -- введите новые', ['value' => '', 'placeholder' => 'Например: сериалы, компьютеры, пытки и казни']),
            myFormInput('age', 'Возраст', ['type'=>'number', 'step'=>1, 'value' => $params['age'], 'min' => UsersModel::MIN_AGE, 'max' => UsersModel::MAX_AGE]),
        ];

        if ($params['i_agree_to_read_my_gps']) {
            $formInputs[] = myFormInput('gps_x', 'GPS (широта)', ['type'=>'number', 'step'=>0.00001, 'value' => $params['gps_x'], 'min' => -180, 'max' => 180]);
            $formInputs[] = myFormInput('gps_y', 'GPS (широта)', ['type'=>'number', 'step'=>0.00001, 'value' => $params['gps_y'], 'min' => -90, 'max' => 90]);
            ?>

            <script>
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition);
                }
                function showPosition(position) {
                    jQuery('[name=gps_x]').val(position.coords.longitude.toFixed(5));
                    jQuery('[name=gps_y]').val(position.coords.latitude.toFixed(5));
                }
            </script>

            <?php
        } else {
            foreach (myGetGpsByGeoIp($_SERVER['REMOTE_ADDR']) as $key => $value) {
                $formInputs[] = "<input type='hidden' name='{$key}' value='{$value}' />";
            }
        }

        foreach ([1, 2, 3, 4, 5] as $i) {
            $formInputs[] = myFormInputPhoto("photo{$i}", "Фотография {$i}", ['value' => $params["photo{$i}"]]);
        }

        foreach ($formInputs as $field) {
            echo $field;
        }
    ?>

    <input type="submit" value="Сменить данные">
</form>