<h1>Список пользователей</h1>
<?php $isAuthorized = !empty((new UsersModel())->getUserIdFromSession()); ?>

<table class="table">
    <thead>
        <th>Фото</th>
        <th>Имя</th>
        <th>Гендер</th>
        <th>Рейтинг</th>
        <th>Интересы</th>
        <?php if ($isAuthorized) {
            echo "
        <th>Общие (с моими) интересы</th>
        <th>Расстояние от меня</th>";
        } ?>
        <th>Действия</th>
    </thead>
<?php

foreach ($params['user_list'] as $user) {
    $photo = $user['photo_main'];
    $gender = UsersModel::genders[$user['gender']];
    $actions = [];

    if ($isAuthorized) {
        $actions[] = "<a href='/index.php?controller=users&action=profile&id={$user['id']}'>Просмотреть профиль</a>";
    }
    echo "
    <tr>
        <td style='text-align: right'><img src='{$photo}' style='height: 6em;'></td> <!-- Фото -->
        <td>{$user['full_name']}</td> <!-- Имя -->
        <td>{$gender}</td> <!-- Гендер -->
        <td>{$user['fame_rating']}</td> <!-- Рейтинг -->
        <td>".join(', ', $user['tags'])."</td> <!-- Интересы -->";
    if ($isAuthorized) {
        echo "
        <td>".join(', ', $user['common_tags'])."</td> <!-- Общие (с моими) интересы -->
        <td>{$user['distance_from_me']} км</td> <!-- Расстояние от меня -->";
    }
    echo "
        <td>
            ". join(",\n", $actions) ."
         </td> <!-- Действия -->
    </tr>
    ";
}

?>
</table>

<form method="get" action="" class="form">
    <input type="hidden" name="controller" value="users" />
    <input type="hidden" name="action" value="propose" />

    <?php
    $sortConditionOptions = [
        UsersModel::SORT_CONDITION_AGE => 'Возраст (сначала молодые)',
        UsersModel::SORT_CONDITION_GPS => 'Расстояние (сначала ближние)',
        UsersModel::SORT_CONDITION_TAGS => 'Кол-во общих интересов',
        UsersModel::SORT_CONDITION_RATING => 'Рейтинг',
    ];

    echo myFormInputSelect('sort_condition',
        "Параметр, по которому сортируем",
        $sortConditionOptions,
        $_GET['sort_condition'] ?? UsersModel::SORT_CONDITION_RATING
    );

    echo "<h2>Параметры для фильтрации</h2>";

    echo myFormInputSelectMultiple('tags_search[]', 'Интересы (tags) -- выберите уже существующий',
        (new TagsModel())->getAllTags());

    echo myFormInput('min_rating', 'Минимальный рейтинг', [
        'value' => $_GET['min_rating'] ?? 0,
        'type' => 'number',
        'min' => 0,
    ]);
    echo myFormInput('max_distance_in_km', 'Максимальное расстояние от меня (в км)', [
        'value' => $_GET['max_distance_in_km'] ?? 40000,
        'type' => 'number',
        'min' => 0,
    ]);
    echo myFormInput('min_age', 'Минимальный возраст', [
        'value' => $_GET['min_age'] ?? UsersModel::MIN_AGE,
        'type' => 'number',
        'min' => UsersModel::MIN_AGE,
        'max' => UsersModel::MAX_AGE,
    ]);
    echo myFormInput('max_age', 'Минимальный возраст', [
        'value' => $_GET['max_age'] ?? UsersModel::MAX_AGE,
        'type' => 'number',
        'min' => UsersModel::MIN_AGE,
        'max' => UsersModel::MAX_AGE,
    ]);
    echo myFormInput('min_count_common_tags', 'Минимальное количество общих интересов', [
        'value' => $_GET['min_count_common_tags'] ?? 0,
        'type' => 'number',
        'min' => 0,
    ]);
    ?>
    <input type="submit" value="Применить" class="btn btn-success" />
</form>
