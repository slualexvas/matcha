<h1><?php echo is_a($params, "ValidatorException") ? "Вы ввели неверные данные" : "Ошибка"; ?></h1>
<p>Сообщение об ошибке:</p>
<div class="jumbotron alert-danger">
<?php echo $params->getMessage(); ?>
</div>
<?php if(is_a($params, "ValidatorException")) { ?>
<p><b><?php echo SITE_NAME; ?></b> предлагает вам нажать ссылку <a href="<?php echo myJavascriptBackLinkHref();?>">Назад</a> и исправить данные.</p>
<?php } ?>
<?php if(is_a($params, "DbException")) { ?>
    <p><b>Запрос:</b> <pre><?php echo $params->getQuery(); ?></pre></p>
<?php } ?>
