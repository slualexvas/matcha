<h1>Мои чаты</h1>

<table class="table" width="100%">
    <thead>
        <th>Пользователь</th>
        <th>Новых сообщений</th>
        <th>Ссылка на чат</th>
    </thead>
<?php
foreach ($params as $chat) {
    echo "
    <tr>
        <td><a href='/index.php?controller=users&action=profile&id={$chat['his_id']}'>{$chat['his_name']}</a></td>
        <td>{$chat['messages_unread']}</td>
        <td><a href='/index.php?controller=chat&action=view&user_id={$chat['his_id']}' class='btn btn-success'>Перейти</a></td>
    </tr>";
}
?>
</table>
