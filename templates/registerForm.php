<form method="post" action="/index.php?controller=users&action=register">
    <h1>Форма регистрации</h1>

    <?php echo myFormInput('login', 'Желаемый логин'); ?>
    <?php echo myFormInput('password', 'Желаемый пароль', ['type' => 'password']); ?>
    <?php echo myFormInput('password2', 'Ещё раз пароль', ['type' => 'password']); ?>
    <?php echo myFormInput('email', 'Ваш email', ['type' => 'email']); ?>

    <input type="submit" value="Зарегистрироваться">
</form>