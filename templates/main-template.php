<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title><?php echo $page->getTitle(); ?></title>
    <link rel="stylesheet" href="/templates/bootstrap.css">
    <link rel="stylesheet" href="/templates/main-template.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="/templates/jquery.js"></script>
</head>
<body>
<?php $isAuthorized = !empty((new UsersModel())->getUserIdFromSession()); ?>
    <div class="menu row">
        <a class="brand col-sm-3 btn" href="/">Matcha</a>
        <a class="col-sm-3 btn btn-outline-primary" href="/index.php?controller=users&action=list">Список пользователей</a>
        <?php
        if ($isAuthorized) {
        echo "<a  class=\"col-sm-3 btn btn-outline-primary\" href='/index.php?controller=users&action=profile'>Мой профиль</a>";
        echo "<a  class=\"col-sm-3 btn btn-outline-primary\" href='/index.php?controller=users&action=propose'>Предлагаемые любовнички</a>";
        ?>
        <script>
            $(document).ready(function(){
                let id = <?=(new UsersModel())->getUserIdFromSession();?>;
                sendUpdate ();
                function sendUpdate () {
                    $.post('/index.php?controller=users&action=lastActive&id=' + id, 'json');
                }
                setInterval(sendUpdate, 60000);
            });
        </script>
        <?php
            }
        ?>
    </div>

    <div class="content container-fluid">
        <?php echo $page->content; ?>
    </div>

    <footer class="navbar bg-light users-data">
        <?php echo (new UsersController())->strLoginForm(); ?>
    </footer>
</body>
</html>