<h1>Чат</h1>
<?php $view = new ChatView(); ?>
<div class="row">
    <div class="col-sm-2">
        <h2>Вы</h2>
        <?php echo $view->strUserData($params['my_data']); ?>
    </div>
    <div class="col-sm-8" id="messages_container" style="height: 400px; overflow-y: scroll"></div>
    <div class="col-sm-2">
        <h2>Собеседник</h2>
        <?php echo $view->strUserData($params['his_data']); ?>
    </div>
</div>
<form class="row" method="post" id="send_message_form">
    <input type="text" id="new_message_text" class="col-sm-10" on/>
    <input type="submit" value="Отправить сообщение" class="col-sm-2 btn btn-success">
</form>
<div class="row alert alert-danger" id="error_message"></div>

<script>
    var myId = <?php echo $params['my_data']['id']; ?>;
    var hisId = <?php echo $params['his_data']['id']; ?>;
    var hisName = '<?php echo addslashes($params['his_data']['first_name']); ?>';
    var errorMessageDiv = $('#error_message');

    errorMessageDiv.hide();

    function printMessages(messages) {
        var messagesContainer = $('#messages_container');
        var messagesCountOld = messagesContainer.children().length;
        var messagesCountNew = messages.length;

        // 1) Удалить весь HTML из container;
        messagesContainer.empty();

        // 2) В цикле по сообщениям подобавлять HTML.
        var html = "";
        messages.forEach(function (message) {
            var isMessageFromMe = (message.id_from == myId);
            var messageClass = isMessageFromMe ? 'from_me' : 'to_me';
            var senderName = isMessageFromMe ? 'Вы' : hisName;

            if (message.time_of_receive == null) {
                message.time_of_receive = '(Ещё не прочитан)';
            }

            html += "\n<div class='message " + messageClass + "'>" +
                message.message + " <br>" +
                "<div class='datetime'>(<b>Отправлен:</b> " + message.time_of_send + ")," +
                "(<b>Прочитан:</b> " + message.time_of_receive + "),</div>" +
                "</div> <!-- end of message -->\n";
        });
        messagesContainer.append(html);

        // 3) При необходимости сделать скролл вниз
        if (messagesCountOld != messagesCountNew) {
            messagesContainer.scrollTop(messagesContainer[0].scrollHeight);
        }
    }
    function loadMessages() {
        $.get('/index.php?controller=chat&action=getMessages&user_id=' + hisId, printMessages, 'json');
    }
    loadMessages();
    setInterval(loadMessages, 1000);

    $("#send_message_form").submit(function (event) {
        event.preventDefault();

        var msgText = $("#new_message_text").val();
        if (msgText == '') {
            return ;
        }

        $.post('/index.php?controller=chat&action=sendMessage&user_id=' + hisId, {'text': msgText}, function (result) {
            if (result.status) {
                errorMessageDiv.hide();
                loadMessages();
            } else {
                errorMessageDiv.show();
                errorMessageDiv.html(result.message);
            }
        }, 'json');
    });
</script>

<?php echo "<p>Страница обновлена ".date("H:i:s")."</p>"; ?>