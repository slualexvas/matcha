<div>
    Вы залогинились как <b><?php echo $params['login']; ?></b>.
    Вы можете:
    <a href="/index.php?controller=users&action=logout">Разлогиниться</a>,
    <a href="/index.php?controller=users&action=changeUserDataForm">Сменить свои данные</a>,
    <a href="/index.php?controller=chat&action=myChats">Просмотреть свои чаты
        <span id="unreads_count" class="btn btn-danger"></span></a>

    <a href="/index.php?controller=notification&action=notification">Оповещения
        <span id="unread_notifications" class="btn btn-danger"></span></a>
</div>

<script>
    var unreadsCounter = $("#unreads_count");
    var unreadNotifications = $("#unread_notifications");

    function updateUnreadsCount(data) {
        if (data.count > 0) {
            unreadsCounter.show();
            unreadsCounter.html(data.count);
        } else {
            unreadsCounter.hide();
        }
    }

    function updateUnreadNotification(data) {
        if (data.count > 0) {
            unreadNotifications.show();
            unreadNotifications.html(data.count);
        } else {
            unreadNotifications.hide();
        }
    }

    function loadUnreadsCount() {
        $.get('/index.php?controller=chat&action=unreadsCount', updateUnreadsCount, 'json');
        $.get('/index.php?controller=notification&action=unreadNotifications', updateUnreadNotification, 'json');
    }

    unreadsCounter.hide();
    unreadNotifications.hide();
    setInterval(loadUnreadsCount, 1000);
</script>