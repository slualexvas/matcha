<?php
require_once __DIR__."/../const.php";
require_once DIR_ROOT."/autoload.php";
require_once DIR_ROOT."/program-files/functions.php";

function createTable($db, $tableName, $createTableQuery, $insertDataSql, &$stepIndex)
{
    try {
        $db->query("DROP TABLE IF EXISTS {$tableName}");
        $db->query($createTableQuery);
        $db->query($insertDataSql);
    } catch (DbException $e) {
        echo "Error by creating table '{$tableName}':\n";
        echo "{$e->getMessage()}\n";
        echo "Query: {$e->getQuery()}\n";
        die;
    }
    
    echoStepExecuted($stepIndex, "SQL table '{$tableName}' created");
}

function echoStepExecuted(&$stepIndex, $msg)
{
    echo "Step {$stepIndex}: {$msg}\n";
    $stepIndex++;
}

function createTableUsers($db, &$stepIndex)
{
    $tableName = 'users';

    $createSql = "
CREATE TABLE {$tableName}(
  id INT AUTO_INCREMENT,
  login VARCHAR(64) NOT NULL UNIQUE,
  password VARCHAR(64) NOT NULL,
  email VARCHAR(64) NOT NULL,
  is_confirmed INT NOT NULL DEFAULT 0,
  last_name VARCHAR(64),
  first_name VARCHAR(64),
  gender ENUM(".join(', ', array_map([$db, 'quote'], array_keys(UsersModel::genders))).") NOT NULL DEFAULT 'bi',
  sexual_preferences TEXT,
  biography TEXT,
  profile_photo_index ENUM ('1', '2', '3', '4', '5') NOT NULL DEFAULT '1',
  i_agree_to_read_my_gps ENUM ('0', '1') NOT NULL DEFAULT '1',
  gps_x DOUBLE NOT NULL DEFAULT 0,
  gps_y DOUBLE NOT NULL DEFAULT 0,
  age INT NOT NULL DEFAULT 80,
  fame_rating DOUBLE NOT NULL DEFAULT 0,
  last_activity TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(id)
)";

    $insertSql = "
INSERT INTO {$tableName}(id, login, password, email, is_confirmed, first_name, last_name, gender, sexual_preferences, biography, gps_x, gps_y, age) VALUES
    (1, 'ivanov', ".$db->quote(myPasswordEncrypt('ivanovA7')).", 'angell2006@ukr.net', 1, 'Иван', 'Иванов', 'male', 'Люблю сзади, сверху, но больше всего люблю в мозг', 'Родился в Челябинске в семье слесаря', 50.44949, 30.38596, 20),
    (2, 'petrova', ".$db->quote(myPasswordEncrypt('petrovaA7')).", 'angell2006@ukr.net', 1, 'Маша', 'Петрова', 'female', NULL, NULL, 50.44948, 30.38586, 17),
    (3, 'nevedomo', ".$db->quote(myPasswordEncrypt('nevedomoA7')).", 'angell2006@ukr.net', 1, 'Неведомо', 'Зверушко', 'bi', 'Тентакли', NULL, 0, 0, 25),
    (4, 'negrityanka', ".$db->quote(myPasswordEncrypt('otherpassA7')).", 'angell2006@ukr.net', 1, 'Толу', 'Лулу', 'female', NULL, NULL, 27.91463, 34.21031, 20),
    (5, 'kitayanka', ".$db->quote(myPasswordEncrypt('otherpassA7')).", 'angell2006@ukr.net', 1, 'Цинь', 'Ти', 'female', NULL, NULL, 26.972050, 115.145368, 20),
    (6, 'negr', ".$db->quote(myPasswordEncrypt('otherpassA7')).", 'angell2006@ukr.net', 1, 'Чорный', 'Властелин', 'male', NULL, NULL, 27.91453, 34.21021, 30),
    (7, 'kitaec', ".$db->quote(myPasswordEncrypt('otherpassA7')).", 'angell2006@ukr.net', 1, 'Очки', 'Ннада', 'male', NULL, NULL, 26.972040, 115.145358, 30),
    (8, 'babka', ".$db->quote(myPasswordEncrypt('otherpassA7')).", 'angell2006@ukr.net', 1, 'Бабка', 'Старая', 'female', NULL, NULL, 50.44948, 30.38586, 80),
    (9, 'not_filled_data', ".$db->quote(myPasswordEncrypt('otherpassA7')).", 'angell2006@ukr.net', 1, NULL, NULL, 'bi', NULL, NULL, 0, 0, 80)
    ";

    createTable($db, $tableName, $createSql, $insertSql, $stepIndex);
}

function createTableTags($db, &$stepIndex)
{
    $tableName = 'tags';

    $createSql = "
CREATE TABLE {$tableName}(
  id INT AUTO_INCREMENT,
  tag VARCHAR(64) NOT NULL UNIQUE,
  PRIMARY KEY(id)
)";

    $insertSql = "
INSERT INTO {$tableName}(id, tag) VALUES
    (1, 'цветы'),
    (2, 'мотоциклы'),
    (3, 'сериалы'),
    (4, 'пытки и казни')
    ";

    createTable($db, $tableName, $createSql, $insertSql, $stepIndex);
}

function createTableUsersTagsRel($db, &$stepIndex)
{
    $tableName = 'users_tags_rel';

    $createSql = "
CREATE TABLE {$tableName}(
  id_user INT NOT NULL,
  id_tag INT NOT NULL,
  PRIMARY KEY(id_user, id_tag),
  FOREIGN KEY (id_user) REFERENCES users(id) ON DELETE CASCADE,
  FOREIGN KEY (id_tag) REFERENCES tags(id) ON DELETE CASCADE
)";

    $insertSql = "
INSERT INTO {$tableName}(id_user, id_tag) VALUES
  (1, 2),
  (1, 3),
  (2, 1),
  (2, 3),
  (4, 2),
  (4, 3),
  (4, 1)
    ";

    createTable($db, $tableName, $createSql, $insertSql, $stepIndex);
}

function createTableViewHistory($db, &$stepIndex)
{
    $tableName = 'view_history';

    $createSql = "
CREATE TABLE {$tableName}(
  id INT AUTO_INCREMENT,
  id_viewer INT NOT NULL,
  id_viewed INT NOT NULL,
  time_of_viewing DATETIME NOT NULL DEFAULT NOW(),
  PRIMARY KEY(id),
  UNIQUE(id_viewer, id_viewed, time_of_viewing)
)";

    $insertSql = "
INSERT INTO {$tableName}(id_viewer, id_viewed) VALUES
  (1, 2), -- Иванов смотрел Петрову,
  (1, 3), -- Иванов смотрел Неведомо Зверушко,
  (2, 3)  -- Петрова смотрела Неведомо Зверушко
    ";

    createTable($db, $tableName, $createSql, $insertSql, $stepIndex);
}

function createTableLikes($db, &$stepIndex)
{
    $tableName = 'likes';

    $createSql = "
CREATE TABLE {$tableName}(
  id_liker INT NOT NULL,
  id_liked INT NOT NULL,
  UNIQUE(id_liker, id_liked)
)";

    $insertSql = "
INSERT INTO {$tableName}(id_liker, id_liked) VALUES
  (1, 2), -- Иванов лайкнул Петрову,
  (2, 1), -- Петрова лайкнула Иванова,
  (2, 3), -- Петрова лайкнула Неведомо Зверушко
  (2, 5), -- Петрова лайкнула Цинь Ти
  (1, 5), -- Иванов лайкнул Цинь Ти
  (3, 5)  -- Зверушко лайкнуло Цинь Ти
    ";

    createTable($db, $tableName, $createSql, $insertSql, $stepIndex);
}

function createTableChat($db, &$stepIndex)
{
    $tableName = 'chat';

    $createSql = "
CREATE TABLE {$tableName}(
  id INT AUTO_INCREMENT,
  id_from INT NOT NULL,
  id_to INT NOT NULL,
  time_of_send DATETIME NOT NULL DEFAULT NOW(),
  time_of_receive DATETIME NULL DEFAULT NULL,
  message VARCHAR(512) NOT NULL DEFAULT '',
  PRIMARY KEY(id)
)";

    $insertSql = "
INSERT INTO {$tableName}(id_from, id_to, message) VALUES
  (1, 2, 'Маша, привет! Как дела?'),
  (2, 1, 'Ваня, привет! Спасибо, хорошо!'),
  (1, 2, 'Маша, пошли гулять сегодня вечером?')
    ";

    createTable($db, $tableName, $createSql, $insertSql, $stepIndex);
}

function createTableUsersBlock($db, &$stepIndex)
{
    $tableName = 'users_block';

    $createSql = "
CREATE TABLE {$tableName}(
  id_blocker INT NOT NULL,
  id_blocked INT NOT NULL,
  UNIQUE(id_blocker, id_blocked),
  FOREIGN KEY (id_blocker) REFERENCES users(id) ON DELETE CASCADE,
  FOREIGN KEY (id_blocked) REFERENCES users(id) ON DELETE CASCADE
)";

    $insertSql = "
INSERT INTO {$tableName}(id_blocker, id_blocked) VALUES
  (1, 3)
    ";

    createTable($db, $tableName, $createSql, $insertSql, $stepIndex);
}

function createTableNotification($db, &$stepIndex)
{
    $tableName = 'notification';

    $createSql = "
CREATE TABLE {$tableName}(
  id INT NOT NULL AUTO_INCREMENT, 
  user_id INT NOT NULL, 
  text VARCHAR(255) NOT NULL, 
  is_read BOOLEAN NOT NULL DEFAULT FALSE, 
  n_time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
  PRIMARY KEY (id)
)";

    $insertSql = "
INSERT INTO {$tableName}(user_id, text) VALUES
  (1, 'Пользователь Маша Петрова лайкнул вас')
    ";

    createTable($db, $tableName, $createSql, $insertSql, $stepIndex);
}


$stepIndex = 1;
$db = DbDecorator::getInstance();
$db->query("SET FOREIGN_KEY_CHECKS = 0");

createTableUsers($db, $stepIndex);
createTableTags($db, $stepIndex);
createTableUsersTagsRel($db, $stepIndex);
createTableViewHistory($db, $stepIndex);
createTableLikes($db, $stepIndex);
createTableChat($db, $stepIndex);
createTableUsersBlock($db,$stepIndex);
createTableNotification($db,$stepIndex);

$filesPath = DIR_ROOT . '/files';

//$cmd = "chown -R slualexvas.slualexvas {$filesPath}";
//echo `$cmd`;

$cmd = "chmod -R 0777 {$filesPath}";
echo `$cmd`;
echoStepExecuted($stepIndex, "'{$cmd}' executed");

$usersModel = new UsersModel();
$usersModel->recalcFameRating();
echoStepExecuted($stepIndex, "fame_rating recalculated");

$db->query("SET FOREIGN_KEY_CHECKS = 1");
echo "Setup complete\n";